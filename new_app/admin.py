from django.contrib import admin
from .models import NewModel

class NewModelAdmin(admin.ModelAdmin):
    list_display = ('name','title')
    search_fields = ('name','title')

    filter_horizontal = ()
    ordering = ()
    list_filter = ()
    fieldsets = ()

admin.site.register(NewModel, NewModelAdmin)
