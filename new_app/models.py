from django.db import models


class NewModel(models.Model):
    title = models.CharField('title', max_length=155)
    name = models.CharField('name', max_length=155)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'new_model'
        verbose_name_plural = 'new_models'
